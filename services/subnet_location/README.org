#+Title: Location based on subnet

Map a list of ip subnets to location IDs.

Example Data:

{
  "PPAZ": [ "138.197.66.171", "138.196.0.0/8" ],
  "USDC": [ "192.168.42.0/24", "192.168.1.0/24" ]
}
